# Wagtail Cloudinary Image

This package provides a custom WagtailImage model that leverages django-cloudinary-storage. It replaces Wagtail's usage of Willow with Cloudinary transformations. As such, Wagtail Image Renditions no longer need to save a separate file as transformed images come directly from Cloudinary.

Without this package, django-cloudinary-storage uploads a new copy of the image for each wagtail transform operation and processes them with Willow. This partially defeats the point of using Cloudinary.

![](./wagtail_image_edit.png)

# Install

1. Add wagtail-cloudinary-image using your favorite Python package manager. For example `poetry add wagtail-cloudinary-image`.
2. Set required CLOUDINARY_STORAGE fields as documented [here](https://github.com/klis87/django-cloudinary-storage#settings)
3. Add `cloudinary`, `cloudinary_storage`, and `wagtail_cloudinary_image` to your `INSTALLED_APPS`, keeping in mind the ordering concerns listed [here](https://github.com/klis87/django-cloudinary-storage#installation) and putting `wagtail_cloudinary_image` after the others.
4. Set `WAGTAILIMAGES_IMAGE_MODEL = "wagtail_cloudinary_image.CloudinaryImage"`

This package can be used without setting STATICFILES_STORAGE or DEFAULT_FILE_STORAGE. Of course, you could choose to use django-cloudinary-storage for static and non-wagtail media as well.

# Things to improve

- Only the MinMaxOperation is supported. If others were supported, it would be possible to drop in this package in an existing Wagtail site and everything would just work.
- There is no way to import Cloudinary images into Wagtail.

# Usage

When referencing a image in a Wagtail Page model, use get_image_model_string

```
from wagtail.images import get_image_model_string

class ExamplePage(Page):
    image = models.ForeignKey(
        get_image_model_string(),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
```

Django templates work as normal, but only for supported operations.

```
{% load wagtailimages_tags %}
{% image page.image max-400x400 %}
```

API usage should work as normal. You may use renditions if you prefer controlling image transformations server side. You may prefer to send only the cloudinary name and let the client deal with tranformations.

## Extending base models

1. Import `wagtail_cloudinary_image.abstract.AbstractCloudinaryImage` and extend it
2. Do not include `wagtail_cloudinary_image` in your `INSTALLED_APPS`
3. Set `WAGTAILIMAGES_IMAGE_MODEL` to your new model that extends the abstract class

# Developing

You'll need a Cloudinary account and API key. Our preferred development environment is Docker but you could edit sandbox/settings.py if preferred.

1. `cp docker-compose.yml docker-compose.override.yml`
2. Edit docker-compose.override.yml and add your API key
3. `docker-compose up`
4. `docker-compose run --rm web ./manage.py migrate`

## Testing

Tests require a valid Cloudinary API Key. As such, they will only be run on the master branch and release tags `v*`. When submitting a merge request, ensure you run tests locally with your API key as they will not run on CI without them.
